import { Buffer } from 'buffer';
import { getBIP44AddressKeyDeriver } from '@metamask/key-tree';
import { OnRpcRequestHandler } from '@metamask/snaps-types';
import { heading, panel, text } from '@metamask/snaps-ui';
import BN from 'bn.js';
import { privateKeyToAccountAddress, signTransaction } from './sign';

/**
 * Handle incoming JSON-RPC requests, sent through `wallet_invokeSnap`.
 *
 * @param args - The request handler args as object.
 * @param args.request - A validated JSON-RPC request object.
 * @returns The result of request.
 * @throws If the request method is not valid for this snap.
 */
export const onRpcRequest: OnRpcRequestHandler = async ({ request }) => {
  switch (request.method) {
    case 'get_address': {
      const privateKey = await getPrivateKey();
      return privateKeyToAccountAddress(privateKey);
    }

    case 'sign_transaction': {
      const { params } = request;
      if (validSignParams(params)) {
        const { chainId } = params;
        const txPayload: Buffer = Buffer.from(params.payload, 'hex');
        if (txPayload.length < 45) {
          throw new Error('Incomplete transaction');
        }

        const nonce = new BN(txPayload.subarray(0, 8), 'be');
        const gasCost = new BN(txPayload.subarray(16, 24), 'be');
        const target = txPayload.toString('hex', 24, 45);
        const userConfirmed = await snap.request({
          method: 'snap_dialog',
          params: {
            type: 'confirmation',
            content: panel([
              heading(`Confirm transaction signature on ${chainId}`),
              text(
                `Signing transaction with nonce ${nonce} and cost ${gasCost} towards contract ${target}`,
              ),
            ]),
          },
        });
        if (userConfirmed === true) {
          const privateKey = await getPrivateKey();
          return signTransaction(txPayload, chainId, privateKey);
        }
        throw new Error('User rejected signing');
      } else {
        throw new Error('Malformed sign request');
      }
    }
    default:
      throw new Error('Method not found.');
  }
};

type SignParams = {
  payload: string;
  chainId: string;
};

/**
 * Verify the format of parameters passed to sign method.
 *
 * @param params - The params to verify.
 * @returns True if params has valid format.
 */
function validSignParams(params: unknown): params is SignParams {
  return (
    params !== null &&
    params !== undefined &&
    typeof params === 'object' &&
    'payload' in params &&
    'chainId' in params
  );
}

/**
 * Get the private key from wallet.
 *
 * @returns The private key.
 */
async function getPrivateKey(): Promise<string> {
  const keyEntropy = await snap.request({
    method: 'snap_getBip44Entropy',
    params: {
      coinType: 3757,
    },
  });
  const keyDeriver = await getBIP44AddressKeyDeriver(keyEntropy);
  const derivedBip44Node = await keyDeriver(0);
  const { privateKey } = derivedBip44Node;
  if (privateKey === undefined) {
    throw new Error('Unable to get private key');
  }
  return privateKey.replace(/^0x/u, '');
}
