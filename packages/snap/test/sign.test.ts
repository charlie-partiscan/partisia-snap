import BN from 'bn.js';
import { ec as Elliptic } from 'elliptic';
import {
  privateKeyToAccountAddress,
  signatureToBuffer,
  signTransaction,
} from '../src/sign';

describe('Sign tools', () => {
  it('privateKeyToAccountAddress', () => {
    expect(privateKeyToAccountAddress('01')).toBe(
      '0035e97a5e078a5a0f28ec96d547bfee9ace803ac0',
    );

    expect(
      privateKeyToAccountAddress(
        '42834300692d84df068784edb6e26a201022f014631ce20ff189dc969f581953',
      ),
    ).toBe('0056dabb8c650c2119335489e5eaf4a48023574955');
  });

  it('signatureToBuffer', () => {
    expect(() =>
      signatureToBuffer({
        r: new BN(1),
        s: new BN(1),
        recoveryParam: null,
      } as Elliptic.Signature),
    ).toThrow('Recovery parameter is null');
  });

  it('signTransaction', () => {
    const signature = signTransaction(
      Buffer.from('0000000000', 'hex'),
      'Partisia Blockchain',
      '112233',
    );
    expect(signature).toBe(
      '00c60a53ba9b2e734efeb36bafc3977eff78f2a5530d177e587f05de57e63f2776c6c0fdfa91690ee76f7986db270c185159725667b29e0adec019a631aa515f9a',
    );
  });
});
