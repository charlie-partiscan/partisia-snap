import { SnapsGlobalObject } from '@metamask/rpc-methods';

const customGlobal: { snap: SnapsGlobalObject } = global as unknown as {
  snap: SnapsGlobalObject;
};
export const snapRequest = jest.fn();
customGlobal.snap = {
  request: snapRequest,
};
