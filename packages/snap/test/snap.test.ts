import { getBIP44AddressKeyDeriver } from '@metamask/key-tree';
import { Json } from '@metamask/snaps-types';
import { onRpcRequest } from '../src';
import { snapRequest } from './SetupTests';

const origin = 'TestOrigin';

jest.mock('@metamask/key-tree');
const bip44KeyDeriverMock = getBIP44AddressKeyDeriver as jest.Mock;

describe('Snap get address', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('get_address', async () => {
    bip44KeyDeriverMock.mockResolvedValueOnce(() => ({
      privateKey: '0x11223344',
    }));

    const result = await onRpcRequest({
      origin,
      request: {
        id: null,
        method: 'get_address',
        jsonrpc: '2.0',
        params: {},
      },
    });
    expect(result).toBe('00d7e42ba9e815a3040584be2b4509e657e6601a3a');

    expect(snap.request).toHaveBeenCalledWith({
      method: 'snap_getBip44Entropy',
      params: {
        coinType: 3757,
      },
    });
  });

  it('rejects when unable to get private key', async () => {
    bip44KeyDeriverMock.mockResolvedValueOnce(() => ({
      privateKey: undefined,
    }));

    await expect(
      onRpcRequest({
        origin,
        request: {
          id: null,
          method: 'get_address',
          jsonrpc: '2.0',
          params: {},
        },
      }),
    ).rejects.toThrow('Unable to get private key');
  });
});

describe('Snap unknown method', () => {
  it('unknown method', async () => {
    await expect(
      onRpcRequest({
        origin,
        request: {
          id: null,
          method: 'unknown',
          jsonrpc: '2.0',
          params: {},
        },
      }),
    ).rejects.toThrow('Method not found.');
  });
});

describe('Snap sign transaction', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  beforeEach(() => {
    bip44KeyDeriverMock.mockResolvedValueOnce(() => ({
      privateKey: '0x11223344',
    }));
  });

  it('reject too short payload', async () => {
    await expect(
      onRpcRequest({
        origin,
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: {
            payload: '0'.repeat(88),
            chainId: 'ChainId',
          },
        },
      }),
    ).rejects.toThrow('Incomplete transaction');
  });

  it('reject on user reject', async () => {
    snapRequest.mockResolvedValue(false);
    await expect(
      onRpcRequest({
        origin,
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: {
            payload: '0'.repeat(90),
            chainId: 'ChainId',
          },
        },
      }),
    ).rejects.toThrow('User rejected signing');
  });

  it('signs transaction', async () => {
    snapRequest.mockResolvedValue(true);
    const signature = await onRpcRequest({
      origin,
      request: {
        id: null,
        method: 'sign_transaction',
        jsonrpc: '2.0',
        params: {
          payload: '0'.repeat(90),
          chainId: 'ChainId',
        },
      },
    });
    expect(signature).toBe(
      '011dee4b773af0cd9ebb57f738b5b11744d21117e2d98656f04486caa6beb83f166c35b5f5e0f9f60b41708fb4c1e0f9fd0b5f6e94317f9f1cb3bfe363f4736d0c',
    );
  });

  it('reject malformed paylod', async () => {
    await expect(
      onRpcRequest({
        origin,
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: {
            payload: '0'.repeat(90),
          },
        },
      }),
    ).rejects.toThrow('Malformed sign request');

    await expect(
      onRpcRequest({
        origin,
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: {
            chainId: 'ChainId',
          },
        },
      }),
    ).rejects.toThrow('Malformed sign request');

    await expect(
      onRpcRequest({
        origin,
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: [],
        },
      }),
    ).rejects.toThrow('Malformed sign request');

    await expect(
      onRpcRequest({
        origin,
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: undefined as unknown as Record<string, Json>,
        },
      }),
    ).rejects.toThrow('Malformed sign request');

    await expect(
      onRpcRequest({
        origin,
        request: {
          id: null,
          method: 'sign_transaction',
          jsonrpc: '2.0',
          params: null as unknown as Record<string, Json>,
        },
      }),
    ).rejects.toThrow('Malformed sign request');
  });
});
