# Development site for Partisia Blockchain snap

Development site allowing to test the Partisia Blockchain snap. The site allows installing the snap
and interacting with the primary functionality.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:8000](http://localhost:8000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
