# Partisia Blockchain Snap

This repostiory contains the Partisia Blockchain snap and a development site for testing the snap.

## How do I use the snap

Are you a dapp developer and end-user you should visit
the [Partisia Blockchain documentation](https://partisiablockchain.gitlab.io/documentation/smart-contracts/integration/metamask-snap-integration.html).

## Getting Started developing the snap

To setup the test environment, serving the snap and starting the development site.

```shell
yarn install && yarn start
```

## Contributing

## Testing and Linting

Run `yarn test` to run the tests once.

Run `yarn lint` to run the linter, or run `yarn lint:fix` to run the linter and fix any
automatically fixable issues.
